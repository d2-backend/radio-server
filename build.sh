shopt -s globstar
rm -rf bin
mkdir bin
javac \
    -classpath "libs/*" \
	-sourcepath :src \
	-d bin \
	src/**/*.java
