var request = function (url, request, callback) {

    var xhr = new XMLHttpRequest();

    xhr.open("POST", url, true);

    xhr.onload = function () {
        callback(JSON.parse(xhr.responseText));
    };

    var requestString = JSON.stringify(request);
    xhr.send(requestString);

};

var dpToPx = function (dp) {
    return dp;
};

var rgbaStringToColor = function (rgbaString) {
    var array = rgbaString.substring(5, rgbaString.length - 1).replace(" ", "").split(",");
    return {
        red: +array[0],
        green: +array[1],
        blue: +array[2],
        alpha: +array[3]
    };
};

var colorToRgbaString = function (color) {
    return "rgba(" + color.red + "," + color.green + "," + color.blue + "," + color.alpha + ")";
};

var createIcon = function (name, color, size) {
    var icon = document.createElement("div");
    icon.style.width = size + "px";
    icon.style.height = size + "px";
    icon.style.background = color;
    icon.style.webkitMaskImage = "url(" + name + ".icon.png)";
    icon.style.webkitMaskSize = "cover";
    return icon;
};

var DEFAULT_CONTENT_COLOR = "rgba(0,0,0,1)";

var getContentColor = function (container) {
    while (true) {
        if (!container.view) return DEFAULT_CONTENT_COLOR;
        if (container.view.contentColor === undefined) {
            container = container.parentElement;
        } else {
            return container.view.contentColor;
        }
    }
};

var DEFAULT_TEXT_SIZE = "12px";

var getTextSize = function (container) {
    while (true) {
        if (!container.view) return DEFAULT_TEXT_SIZE;
        if (container.view.textSize === undefined) {
            container = container.parentElement;
        } else {
            return dpToPx(container.view.textSize) + "px";
        }
    }
};
