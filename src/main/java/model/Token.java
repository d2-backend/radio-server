package main.java.model;

public class Token {
	int id;
	int userId;
	String token;
	long unixTime;
	public final static String TABLE_NAME = "token";
	public final static String FIELD_ID = "id";
	public final static String FIELD_USER_ID = "user_id";
	public final static String FIELD_TOKEN = "token";
	public final static String FIELD_CREATE_AT = "create_at";
}
