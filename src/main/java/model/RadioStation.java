package main.java.model;

import java.util.List;

public class RadioStation {
	private int id;
	private String name;
	private String logo;
	private String desc;
	private String audio;
	private String video;
	private List<Tag> tags;

	public final static String TABLE_NAME = "radiostation";
	public final static String FIELD_ID = "id";
	public final static String FIELD_NAME = "name";
	public final static String FIELD_LOGO = "logo";
	public final static String FIELD_DESCRIPTION = "description";
	public final static String FIELD_AUDIO = "audio";
	public final static String FIELD_VIDEO = "video";
	public final static String FIELD_TAGS = "tags";

	public RadioStation(int id, String name, String logo, String desc, String audio, String video, List<Tag> tags) {
		this.id = id;
		this.name = name;
		this.logo = logo;
		this.desc = desc;
		this.audio = audio;
		this.video = video;
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "RadioStation{" +
				"id=" + id +
				", name='" + name + '\'' +
				", logo='" + logo + '\'' +
				", desc='" + desc + '\'' +
				", audio='" + audio + '\'' +
				", video='" + video + '\'' +
				", tags=" + tags +
				'}';
	}
}
