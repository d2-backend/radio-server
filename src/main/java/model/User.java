package main.java.model;

public class User {
	public static final String TABLE_NAME = "user";
	public static final String FIELD_FACEBOOK_ID = "facebook_id";
	public static final String FIELD_FIRST_NAME = "first_name";
	public static final String FIELD_LAST_NAME = "last_name";
	public static final String FIELD_EMAIL = "email";
	public static final String FIELD_PHOTO = "photo";
	public static final String FIELD_PHONE = "phone";

	private String facebookId;
	private String firstName;
	private String lastName;
	private String email;
	private String photo;
	private String phone;

	public User(String facebookId, String firstName, String lastName, String email, String photo, String phone) {
		this.facebookId = facebookId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.photo = photo;
		this.phone = phone;
	}
}
