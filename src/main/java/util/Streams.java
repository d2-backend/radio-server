package main.java.util;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Streams {
	public static byte[] streamToByteArray(InputStream inputStream) throws IOException {

		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			while (true) {
				int r = inputStream.read(buffer);
				if (r == -1) break;
				out.write(buffer, 0, r);
			}

			return out.toByteArray();

		} finally {
			inputStream.close();
		}

	}

	public static void byteArrayToStream(byte[] bytes, OutputStream outputStream) throws IOException {

		outputStream.write(bytes);

		outputStream.close();

	}

	private static String getCacheFileName(String url) {
		String value = url;
		value = value.replaceAll(":", ".");
		value = value.replaceAll("/", ".");
		return "cache." + value;
	}

	public static void stringToStream(String string, OutputStream stream) throws IOException {
		byteArrayToStream(string.getBytes(), stream);
	}


	public static String streamToString(InputStream inputStream) throws IOException {
		return new String(streamToByteArray(inputStream));
	}
}
