package main.java.servlets;

import main.java.database.CRUD;
import main.java.main.ThisApp;
import main.java.model.RadioStation;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/radiostations")
public class ServletRadioStations extends HttpServlet {
	public ServletRadioStations() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ResultSet resultSet = CRUD.selectAll(RadioStation.TABLE_NAME);
			while (resultSet.next()) {
				System.out.println(new RadioStation(resultSet.getInt(RadioStation.FIELD_ID),
						resultSet.getString(RadioStation.FIELD_NAME),
						resultSet.getString(RadioStation.FIELD_LOGO),
						resultSet.getString(RadioStation.FIELD_DESCRIPTION),
						resultSet.getString(RadioStation.FIELD_AUDIO),
						resultSet.getString(RadioStation.FIELD_VIDEO),
						null)); // TODO: 11/17/2017
			}
		} catch (SQLException e) {
			ThisApp.handleErrors(e);
		}
		List<String> radiostations = new ArrayList<String>();
//		radiostations.add("Radio pop");
//		radiostations.add("Radio folk");
//		radiostations.add("Radio jazz");
//		radiostations.add("Radio metal");
		doSetResult(resp, radiostations);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(req, resp);
	}

	private void doSetResult(HttpServletResponse response, List<String> result) throws IOException {
		JSONArray array = new JSONArray();
		array.put(result);
		response.getOutputStream().write(array.toString().getBytes("UTF-8"));
		response.setContentType("application/json; charset=UTF-8");
		response.setStatus(HttpServletResponse.SC_OK);
	}

	private void doSetError(HttpServletResponse response) throws IOException {
		response.getOutputStream().write("error".getBytes("UTF-8"));
		response.setContentType("application/json; charset=UTF-8");
		response.setStatus(HttpServletResponse.SC_OK);
	}
}
