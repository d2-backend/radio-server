package main.java.servlets;

import main.java.main.Server;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletStop extends HttpServlet {

	private final Server server;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println(req.getRemoteAddr());
		if (!"0:0:0:0:0:0:0:1".equals(req.getRemoteAddr())) return;
		System.out.println(req.getRemoteHost());
		try {
			server.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

	public ServletStop(Server server) {
		this.server = server;
	}
}
