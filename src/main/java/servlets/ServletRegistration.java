package main.java.servlets;

import main.java.database.CRUD;
import main.java.util.Streams;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ServletRegistration extends HttpServlet {
	private static final String TABLE_NAME = "user";
	private static final String FIELD_NAME = "name";
	private static final String FIELD_PASSWORD = "password";
	private static final String JSON_NAME = "name";
	private static final String JSON_PASSWORD = "password";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServletInputStream inputStream = req.getInputStream();
		String s = Streams.streamToString(inputStream);
		JSONObject object = new JSONObject(s);
		Map<String, String> data = new HashMap<>();
		data.put(FIELD_NAME, object.getString(JSON_NAME));
		data.put(FIELD_PASSWORD, object.getString(JSON_PASSWORD));
		try {
			CRUD.insert(TABLE_NAME, data);
			resp.getOutputStream().write("ok".getBytes());
		} catch (SQLException e) {
			resp.getOutputStream().write("error".getBytes());
		}
	}
}
