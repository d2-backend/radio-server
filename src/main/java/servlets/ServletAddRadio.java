package main.java.servlets;


import main.java.database.CRUD;
import main.java.util.Streams;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/addradio")
public class ServletAddRadio extends HttpServlet {
	private static final String TABLE_NAME = "radiostation";
	private static final String FIELD_NAME = "name";
	private static final String FIELD_AUDIO = "audio";
	private static final String FIELD_VIDEO = "video";
	private static final String JSON_NAME = "name";
	private static final String JSON_AUDIO = "audio";
	private static final String JSON_VIDEO = "video";


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String inputStreamToString = Streams.streamToString(req.getInputStream());
		System.out.println(inputStreamToString);
		JSONObject object = new JSONObject(inputStreamToString);
		Map<String, String> data = new HashMap<>();
		data.put(FIELD_NAME, object.getString(JSON_NAME));
		data.put(FIELD_AUDIO, object.getString(JSON_AUDIO));
		data.put(FIELD_VIDEO, object.getString(JSON_VIDEO));
		List<String> fields = new ArrayList<>();
		fields.add(FIELD_AUDIO);
		fields.add(FIELD_NAME);
		try {
			CRUD.insert(TABLE_NAME, data);
//			CRUD.deleteById(TABLE_NAME, 4);
//			CRUD.updateById(TABLE_NAME, data, 2);
//			ResultSet resultSet = CRUD.selectById(TABLE_NAME, fields, 2);
//			resultSet.next();
//			System.out.println(resultSet.getString(2));
//			System.out.println(resultSet.getString(1));
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "ok");
			resp.getOutputStream().write(jsonObject.toString().getBytes()); // TODO: 10/18/2017  answer to js
			System.out.println("ok");
		} catch (SQLException e) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "error");
			resp.getOutputStream().write(jsonObject.toString().getBytes());
			System.out.println("error");
			e.printStackTrace();
		}
	}
}
