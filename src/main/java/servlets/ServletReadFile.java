package main.java.servlets;

import main.java.main.ThisApp;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


@WebServlet("/file")
public class ServletReadFile extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println(System.currentTimeMillis());
		ThisApp.readFileToResponse(resp, "Vojna i mir. Tom 1.txt", "text/plain; charset=UTF-8");
		System.out.println(System.currentTimeMillis());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}
}
