package main.java.servlets;

import main.java.database.CRUD;
import main.java.main.ThisApp;
import main.java.model.User;
import main.java.util.Streams;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ServletAuthorization extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		InputStream inputStream = req.getInputStream();
		JSONObject jsonObject = new JSONObject(Streams.streamToString(inputStream));
		String facebookId = jsonObject.optString("facebookId");
		String firstName = jsonObject.optString("firstName");
		String lastName = jsonObject.optString("lastName");
		String email = jsonObject.optString("email");
		String photo = jsonObject.optString("photo");
		String phone = jsonObject.optString("phone");
		String deviceId = jsonObject.optString("deviceId");
		String token = "" + deviceId.hashCode() + (phone + facebookId).hashCode();
		OutputStream outputStream = resp.getOutputStream();
		Streams.stringToStream(token, outputStream);
		System.out.println("TOKEN: " + token);
		Map<String, String> data = new HashMap<>();
		data.put(User.FIELD_FACEBOOK_ID, facebookId);
		data.put(User.FIELD_FIRST_NAME, firstName);
		data.put(User.FIELD_LAST_NAME, lastName);
		data.put(User.FIELD_EMAIL, email);
		data.put(User.FIELD_PHONE, phone);
		data.put(User.FIELD_PHOTO, photo);
		try {
			CRUD.insert(User.TABLE_NAME, data);
		} catch (SQLException e) {
			ThisApp.handleErrors(e);
		}
		try {
			int id = getUserIdByPhoneFacebook(phone, facebookId);

		} catch (SQLException e) {
			ThisApp.handleErrors(e);
		}
	}

	private static int getUserIdByPhoneFacebook(String phone, String facebookId) throws SQLException {
		Map<String, String> dataId = new HashMap<>();
		dataId.put(User.FIELD_PHONE, phone);
		dataId.put(User.FIELD_FACEBOOK_ID, facebookId);
		int id = CRUD.selectIdByPhoneFacebook(User.TABLE_NAME, dataId);
		System.out.println("ID = " + id);
		return id;
	}
}
