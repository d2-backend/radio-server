package main.java.database;


import main.java.main.ThisApp;
import main.java.model.RadioStation;
import main.java.servlets.ServletRadioStations;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CRUD {
	private static final String USERNAME = "root";
	private static final String PASSWORD = "root";
	private static final String DATABASE = "radioapp";
	private static final String HOST = "localhost";
	//	private static final String HOST = "207.154.193.200";
	private static Connection conn;

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + HOST + "/" + DATABASE + "?user=" + USERNAME + "&password=" + PASSWORD); // TODO: 11/16/2017  port
		} catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
			ThisApp.handleErrors(e);
		}
	}

	public static void fish() {
		try {
			Map<String, String> data = new HashMap<>();
			data.put(RadioStation.FIELD_NAME, "rock");
			data.put(RadioStation.FIELD_DESCRIPTION, "Не следует, однако забывать, что консультация с широким активом позволяет оценить значение систем массового участия. Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции играет важную роль в формировании соответствующий условий активизации");
			data.put(RadioStation.FIELD_AUDIO, "erftret");
			data.put(RadioStation.FIELD_VIDEO, "ghjnrgdhr");
			data.put(RadioStation.FIELD_LOGO, "ghjnrgtergedrf");
			insert(RadioStation.TABLE_NAME, data);
		} catch (SQLException e) {
			ThisApp.handleErrors(e);
		}
	}

	public static void insert(String tableName, Map<String, String> data) throws SQLException {
		StringBuilder values = new StringBuilder();
		StringBuilder fields = new StringBuilder();
		for (Map.Entry<String, String> entry : data.entrySet()) {
			values.append("'").append(entry.getValue()).append("'").append(",");
			fields.append(entry.getKey()).append(",");
		}
		values.deleteCharAt(values.length() - 1);
		fields.deleteCharAt(fields.length() - 1);
		String query = "INSERT INTO " + tableName + "(" + fields + ")" + " VALUES(" + values + ");";
		System.out.println("QUERY " + query);
		PreparedStatement statement = conn.prepareStatement(query);
		statement.execute();
	}

	public static void deleteById(String tableName, int id) throws SQLException {
		String query = "DELETE FROM " + tableName + " WHERE id=" + id;
		PreparedStatement statement = conn.prepareStatement(query);
		statement.execute();
	}

	public static void updateById(String tableName, Map<String, String> data, int id) throws SQLException {
		StringBuilder updates = new StringBuilder();
		for (Map.Entry<String, String> entry : data.entrySet()) {
			updates.append(entry.getKey()).append("='").append(entry.getValue()).append("',");
		}
		updates.deleteCharAt(updates.length() - 1);
		String query = "UPDATE " + tableName + " SET " + updates + " WHERE id=" + id;
		PreparedStatement statement = conn.prepareStatement(query);
		statement.execute();
	}

	public static ResultSet selectAll(String tableName) throws SQLException {
		StringBuilder builder = new StringBuilder();
		String query = "SELECT * FROM " + tableName;
		PreparedStatement statement = conn.prepareStatement(query);
		return statement.executeQuery();
	}

	public static ResultSet selectById(String tableName, List<String> fields, int id) throws SQLException {
		StringBuilder builder = new StringBuilder();
		for (String field : fields) {
			builder.append(field).append(",");
		}
		builder.deleteCharAt(builder.length() - 1);
		String query = "SELECT " + builder + " FROM " + tableName + " WHERE id=" + id;
		PreparedStatement statement = conn.prepareStatement(query);
		return statement.executeQuery();
	}

	public static int selectLastInsertId(String tableName) throws SQLException {
		int id;
		String query = "SELECT LAST_INSERT_ID() FROM " + tableName;
		PreparedStatement statement = conn.prepareStatement(query);
		ResultSet resultSet = statement.executeQuery();
		resultSet.next();
		id = resultSet.getInt(1);
		return id;
	}

	public static int selectIdByPhoneFacebook(String tableName, Map<String, String> data) throws SQLException {
		int id;
		StringBuilder builder = new StringBuilder();
		for (Map.Entry<String, String> entry : data.entrySet()) {
			builder.append(entry.getKey()).append("=").append("'").append(entry.getValue()).append("'");
			builder.append(" AND ");
		}
		builder.delete(builder.lastIndexOf(" AND "), builder.length() - 1);
		String query = "SELECT id FROM " + tableName + " WHERE " + builder;
		PreparedStatement statement = conn.prepareStatement(query);
		ResultSet resultSet = statement.executeQuery();
		resultSet.next();
		id = resultSet.getInt(1);
		return id;
	}
}
