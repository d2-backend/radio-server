package main.java.main;

import main.java.servlets.*;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class Server extends org.eclipse.jetty.server.Server {

	//private Server server;

	@Override
	public org.eclipse.jetty.server.Server getServer() {
		return this;
	}

	public Server(int port) {
		super(port);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		context.addServlet(new ServletHolder(new ServletRadioStations()), "/radiostations");
		context.addServlet(new ServletHolder(new ServletReadFile()), "/file");
		context.addServlet(new ServletHolder(new ServletAddRadiostation()), "/admin/add-new-radiostation");
		context.addServlet(new ServletHolder(new ServletIndexHtml()), "/index.html");
		context.addServlet(new ServletHolder(new ServletUtil()), "/util.js");
		context.addServlet(new ServletHolder(new ServletAddRadio()), "/addradio");
		context.addServlet(new ServletHolder(new ServletRegistration()), "/reg");
		context.addServlet(new ServletHolder(new ServletAuthorization()), "/auth");
		context.addServlet(new ServletHolder(new ServletStop(this)), "/stop");

		HandlerList handlers = new HandlerList();
		handlers.setHandlers(new Handler[]{context});
		setHandler(handlers);

		try {
			start();
			System.out.println("Listening port : " + port);
			join();
		} catch (Exception e) {
			System.out.println("Error.");
			e.printStackTrace();
		}
	}
}
