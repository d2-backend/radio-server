package main.java.main;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;

public class ThisApp {
	public static void handleErrors(Throwable e) {
		System.out.println("THISAPP " + e);
		if (e instanceof IOException)
			System.out.println("IOException");
		else if (e instanceof SQLException)
			System.out.println("SQLException");
		else
			System.out.println(e.getClass());
	}

	public static void readFileToResponse(HttpServletResponse response, String filename, String contentType) throws IOException {
		File file = new File(filename);
		final BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		final ServletOutputStream outputStream = response.getOutputStream();
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType(contentType);
		while (true) {
			final int b = inputStream.read();
			if (b == -1) break;
			outputStream.write(b);
		}
		inputStream.close();
		outputStream.close();
	}
}
