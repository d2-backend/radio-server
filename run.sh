shopt -s globstar
cd public
nohup \
	java \
		-classpath '../libs/*':../bin \
		main.java.main.Main \
&> /dev/null &
$SHELL